# Project 2 Circuit Commerce
Circuit Commerce is our idea of a more modernized computer parts purchasing site. You can view the source code if you wish in the folders above but in order to save resources I'd recommend going to our [Homepage](http://circuitcommercefrontend.s3-website.us-east-2.amazonaws.com/) to view the hosted site

## Some Initial Notes
User registration and password replacement requires use of jenkins. If you would like it see this demonstrated contact me and I would be happy to show you. At any time you can also go back to the homepage by selecting our company logo in the top left of the page.

## Lets Login!
Now there is already a user set with the username: LetsMakeAPurchase and password: 1SecuredP455#
So if you select Login in the headbar you will be brought to the login page. Once we have logged in we can now add to our cart and make a purchase. 

## Lets Edit the User
If you wish we can go to the Profile page by selecting the Account dropdown in the headbar and pressing profile. In this page you can simply select Edit Profile Information on the bottom and add something fun!

## Lets Fill our Cart
Now that you have logged in, lets start adding to our cart. You will see above each item's card there are two symbols one of a shopping cart with a plus sign inside and another of a floppy disk with Wishlist to the right. These are the add to shopping cart and Wishlist buttons respectively. Wishlist functionality was never fully implemented but there is always the potential for that future development. 

Getting back on track. Select a few items you wish to purchase by directly selecting the shopping cart, you will notice that the shopping cart in the top right has a number raised by one each time you add an item. If you wish to view specific computer parts there is a selector at the top with the word 'All' inside. Clicking on this you can sort through categories and view only those. You can see all of the products we have in that category look for the category name and press the arrow and you will see our carousel expand. 

If you wish to see a more detailed view of our products simply select the picture. Here you can see the detailed product page and also add to your cart. 

## Now that we have bought all of our products, lets checkout. 
Now if you remember back to the shopping cart in the top right with the number on it, you can select that at any time to confirm your products. From the shopping cart page you can remove any products you dont wish to have, clear your cart as a whole, and most importantly make a purchase!

Lets hit that button!

Once you have you will be taken to the checkout page you have one more chance to review your products before checking out. In this scenario, we enter the number 4242 4242 4242 4242 for the credit cart, 4/24 for the month and day, 242 for the cvc and the zip code is 42424. Pressing By Now will purchase the order.

## Where did the order go?
If you look at the top headbar, there is a dropdown titled Account. Select that and we can see Orders. The orders page lets us see all previous orders and when they occurred. You can even view your receipt from this page!

# Thankyou
This was a group effort and at any time you can go to my groupmates Github and Gitlab pages by scrolling to the bottom of the site and pressing on the corresponding link. I would like to thank you for spending the time to view my website and would like to thank my teammates for spending the time required to make this a wonderful and educational experience. 

